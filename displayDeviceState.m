%% displayDeviceState(arg)
function [state] = displayDeviceState(arg)
    % Check response consistency and positive acknowledge
    if (arg(1) == 0 && arg(3) == 130 && arg(4) == 0)
        % Check buffer at cell index number 5 to get the state code
        switch(arg(5))
            case 2      % SYS_IDLE: 0x02
                state = 2;
                disp('Current state: SYS_IDLE');
            case 4      % SYS_LOG: 0x04
                state = 4;
                disp('Current state: ');
            case 6      % SYS_TX: 0x06 (streaming 128 Byte)
                state = 6;
                disp('Current state: ');
            case 8      % SYS_TX: 0x08 (streaming 20 Byte)
                state = 8;
                disp('Current state: ');
            otherwise
                state = 0;
                disp('Device state not available.');
        end
    else
        disp('Error on retrieving device state!');
    end
end