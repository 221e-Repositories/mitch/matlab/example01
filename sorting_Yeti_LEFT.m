function out = sorting_Yeti_LEFT(in)

    prs_idx = [9,14,16,13,10,3,15,12,11,2,1,5,7,4,8,6];
    out = in(:,prs_idx);

end