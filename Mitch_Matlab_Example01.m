%% Clear workspace
clear all
clc

global side prsColors pads

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Discover and connect to Mitch device
devlist = blelist;
mitch = ble("mitch_0101");

%% Access custom service and characteristics of interest
cmd = characteristic(mitch, "C8C0A708-E361-4B5E-A365-98FA6B0A836F", ...
    "D5913036-2D8A-41EE-85B9-4E361AA5C8A7");
dat = characteristic(mitch, "C8C0A708-E361-4B5E-A365-98FA6B0A836F", ...
    "09BF2C52-D1D9-C0B7-4145-475964544307");

%% Subscribe to characteristic notify
subscribe(cmd);
subscribe(dat);

%% Get device status (i.e., just to check setup consistency)
% GET_STATE command code (hex): 0x82
write(cmd, [130 0]);    
cmd_response = read(cmd);
state = displayDeviceState(cmd_response);

%% Start data acquisition in STREAMING mode
% To stop acquisition at runtime, write the following command on Command
% Window: write(cmd, [2 1 2]). It set the device in SYS_IDLE state.
if (state == 2)
    % SET_STATE command code (hex): 0x02
    % streaming type: 0xF8 (continuous streaming)
    % stream mode: 0x01 (Pressure only)
    % acquisition freq: 0x01 (5 Hz)
    write(cmd, [2 3 248 1 1]);    
else
    disp('Acquisition already running.');
end

%% Visualize data as Pressure Map

% set the color map to be used
prsColors = parula(256); 

side = 1; % Choose foot side: 0 = right, 1 = left

if (side==0)
    % right foot
    s_pos = [0,0;0,-2;1,-2;2,-2;3,-2.5;0,-3;1,-3;2,-3;3,-3.5;3,-4.5;3,-5.5;3,-7.5;2,-8.5;3,-8.5;1.5,-9.5;2.5,-9.5];
else
    % left foot
    s_pos = [0,0;-3,-2.5;-2,-2;-1,-2;0,-2;-3,-3.5;-2,-3;-1,-3;0,-3;-3,-4.5;-3,-5.5;-3,-7.5;-3,-8.5;-2,-8.5;-2.5,-9.5;-1.5,-9.5];
end

% Set initial position of pads in the map
figure; hold all;
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0.2, 0.1, 0.6, 0.85]);
for i = 1:16
    pads{i} = plot(s_pos(i,1),s_pos(i,2),'sk','MarkerSize',35);
end
ylim([min(s_pos(:,2))-1 max(s_pos(:,2))+1])
axis equal, xticks(''), yticks(''), g=gca; g.XAxis.Visible = 'off';
g.YAxis.Visible = 'off'; g.Color = [.94 .94 .94];

% Assign data callback
dat.DataAvailableFcn = @displayCharacteristicDataPressureMap;

% Window: write(cmd, [2 1 2]). It set the device in SYS_IDLE state.
% write(cmd, [2 1 2]);
