function out = sorting_Yeti_RIGHT(in)

    prs_idx = [4,8,5,2,3,6,7,1,14,16,15,13,9,12,11,10];
    out = in(:,prs_idx);

end