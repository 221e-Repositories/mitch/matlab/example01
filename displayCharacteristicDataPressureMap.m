%% Callback function on data characteristic to manage accerometer decoding 
function displayCharacteristicDataPressureMap(src,evt)
    
    global side prsColors pads

    % Read data
    data = read(src,'oldest');

    prs = data(5:20);
    prs = (abs(prs-255));
    if (side==0)
        pSorted = sorting_Yeti_RIGHT(prs);
    else
        pSorted = sorting_Yeti_LEFT(prs);
    end

    for i = 1:16
        set(pads{i},'MarkerFaceColor',prsColors((pSorted(i)+1),:));
    end

end